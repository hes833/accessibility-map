import xlrd
import xlwt as xlwt

workbook = xlrd.open_workbook('map1.xls')
workbook = xlrd.open_workbook('map1.xls', on_demand = True)
workbook2 = xlrd.open_workbook('map2.xls')
workbook2 = xlrd.open_workbook('map2.xls', on_demand = True)
worksheet = workbook.sheet_by_index(0)
worksheet2 = workbook2.sheet_by_index(0)

practice = []
for col in range(0, worksheet.ncols):
    practice.append([])
    for row in range(0, worksheet.nrows):
        practice[-1].append(worksheet.cell_value(col, row))

distances = {}
for i in range(0, len(practice)):
    connect = {}
    for j in range(0, len(practice[i])):
        if practice[i][j] != 0:
            if practice[i][j] != 100000:
                connect[j+1] = practice[i][j]

    distances[i+1] = connect

practice1 = []
for col in range(0, worksheet2.ncols):
    practice1.append([])
    for row in range(0, worksheet2.nrows):
        practice1[-1].append(worksheet2.cell_value(col, row))


distances1 = {}
for i in range(0, len(practice1)):
    connect = {}
    for j in range(0, len(practice1[i])):
        if practice1[i][j] != 0:
            if practice1[i][j] != 100000:
                connect[j+1] = practice[i][j]

    distances1[i+1] = connect


data = []
data2 = []


def dijkstra(data, start, goal):
    shortest_distance = {}
    predecessor = {}
    unseenNodes = data
    infinity = 999999
    path = []
    for node in unseenNodes:
        shortest_distance[node] = infinity
    shortest_distance[start] = 0

    while unseenNodes:
        minNode = None
        for node in unseenNodes:
            if minNode is None:
                minNode = node
            elif shortest_distance[node] < shortest_distance[minNode]:
                minNode = node
        for childNode, weight in data[minNode].items():
            if weight + shortest_distance[minNode] < shortest_distance[childNode]:
                shortest_distance[childNode] = weight + shortest_distance[minNode]
                predecessor[childNode] = minNode
        unseenNodes.pop(minNode)

    currentNode = goal
    while currentNode != start:
        try:
            path.insert(0, currentNode)
            currentNode = predecessor[currentNode]
        except KeyError:
            print('Path not reachable')
            break
    path.insert(0, start)
    if shortest_distance[goal] != infinity:
        print('Shortest distance is ' + str(shortest_distance[goal]))
        print('And the path is ' + str(path))


def dijkstraAccess(data2, start1, goal):
    access_distance = {}
    predecessor1 = {}
    unseenNodes1 = data2
    infinity = 999999
    path1 = []
    for node in unseenNodes1:
        access_distance[node] = infinity
    access_distance[start1] = 0

    while unseenNodes1:
        minNode = None
        for node in unseenNodes1:
            if minNode is None:
                minNode = node
            elif access_distance[node] < access_distance[minNode]:
                minNode = node
        for childNode, weight in data2[minNode].items():
            if weight + access_distance[minNode] < access_distance[childNode]:
                access_distance[childNode] = weight + access_distance[minNode]
                predecessor1[childNode] = minNode
        unseenNodes1.pop(minNode)

    currentNode = goal
    while currentNode != start1:
        try:
            path1.insert(0, currentNode)
            currentNode = predecessor1[currentNode]
        except KeyError:
            print('Path not reachable')
            break
    path1.insert(0, start1)
    if access_distance[goal] != infinity:
        print('The most accessible distance is ' + str(access_distance[goal]))
        print('And the path is ' + str(path1))


print "\n"
dijkstra(distances, 17, 40)
print "\n"
dijkstraAccess(distances1, 17, 40)

'''
pseudo code for spreadsheet into python
distances = {}
for each row:
    for each column
        get unique identifiers of rows + columns
        add to dictionary if row id != column id
            distances [row id][col id] = d
            distances [col id][row id] = d
            helloooooooooooo
            
more pseudo code:
distances = {}
nrows = worksheet.nrows
ncols = worksheet.ncols

for i in range (1, nrows+1):
    distances[i] = {} #makes another dictionary (nested dictionaries)
    for j in range (1, ncols+1): 
        if not i == j:
            distances[i][j] = worksheet.cell_value(i,j)
'''
